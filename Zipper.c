#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <malloc.h>
#include <fcntl.h>

char *NAME;
char* FULLPATH;
void input_zip(char *dir, char *zip, int arhiv, int bIsFirst);		// Programm for input files into Zip
int read_data(char* fileToRead, int arhiv);
void Arhv(char * PATH, char * Name);
void Rahim(char*PATH);
int main(int argc, char* argv[])
{
    if(argc>1)
    if(strcmp(argv[1],"-a")==0)
    {
        if(argc==4)
        {
            Arhv(argv[2],argv[3]);
        }
        else
        printf("-a *path of file* *Arhive name*");
        
    }
    else if(strcmp(argv[1],"-r")==0)
    {
        if(argc == 3)
            Rahim(argv[2]);
    }
    else 
    {
        printf("-a *path of file* *Arhive name*    for arhivaciya\n -r *path of arhiv*     for razarhivaciya\n");
    }
    else{
                printf("-a *path of file* *Arhive name*    for arhivaciya\n -r *path of arhiv*     for razarhivaciya\n");
    }
    
}
int read_data(char* fileToRead, int arhiv)
{
    int in;
    char buff;
    in = open(fileToRead,O_RDONLY);
    write(arhiv,"\n",1);
    while(read(in,&buff,1)==1)
    {
        write(arhiv,&buff,1);
    }
     write(arhiv,"\n",1);
    close(in);
}

void Arhv(char * PATH, char * Name)
{
    char* FilePath;
if(PATH[strlen(PATH)-1]=='/')
        PATH[strlen(PATH)-1]='\0';
    char* tempChar = malloc(strlen(PATH));
    strcat(tempChar,PATH);
    int tempLen = strlen(tempChar)-1;
    int bIsDeleted=0;
    FULLPATH=malloc(sizeof(char)*2560);
    memset(FULLPATH,'\0',2560);
    while(!bIsDeleted)
    {
        if(tempChar[tempLen] == '/')
        {
            tempChar[tempLen] = '\0';
            bIsDeleted=1;
        }
        else
        {
            tempChar[tempLen] = '\0';
        }
        tempLen--;
        
    }
    int len = strlen(tempChar)+strlen(Name)+5;
    FilePath = malloc(len);
    strcat(FilePath,tempChar);

    strcat(FilePath,"/");
    strcat(FilePath,Name);
    strcat(FilePath,".txt");
    printf("\n%s\n",FilePath);
    int arxiv;
    arxiv = open(FilePath,O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IXOTH|S_IWUSR|S_IWOTH);
    char *dirName=malloc(256);
    strcpy(dirName,strrchr(PATH,'/'));
    NAME=dirName;
    for(int i=0;i<strlen(dirName)-1;i++)
        dirName[i]=dirName[i+1];
    dirName[strlen(dirName)-1]='\0';
    strcat(FULLPATH,tempChar);    strcat(FULLPATH,"/");
    input_zip(tempChar, FilePath,arxiv,0);				// Archiving start
    printf("\nYour archivating was succesfull !\n\n");
    close(arxiv);        
    free(FilePath);
    free(tempChar);
}

//Dir like this /home/*computer name*/lab1/zipperbest



// Func for input files into Zip
void input_zip(char *dir, char *zip, int arhiv, int bIsFirst)		// Programm for input files into Zip
{
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    void*ptr;
    // Конец процесса архивации при ошибке открытия директории
    if((dp = opendir(dir))==NULL)
    {
	 return;
    }
    chdir(dir);
    //printf("%s\n", dir);
    // Archiving into File.zip;
    if(bIsFirst == 0)
    {
        //printf("%s\n",NAME);
        while((entry = readdir(dp)) != NULL)
        {
            lstat(entry->d_name, &statbuf);
            ptr=&statbuf;
            if (S_ISDIR(statbuf.st_mode)) 
            { 
             /* Находит каталог, но игнорирует . и .. */
                if (strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0)
                    continue;
                if(strcmp(entry->d_name,NAME)==0)
                    {
                        
                        write(arhiv,  "d", sizeof(char));
                        char*Name1 = entry->d_name;
                        //printf("\n%s    %ld\n", Name1, strlen(Name1));
                        strcat(FULLPATH,Name1);
                        int len = strlen(FULLPATH);
                        // char s[256]={0};
                        // sprintf(s,"%d",len);
                        write(arhiv,&len,sizeof(len));
                        write(arhiv,FULLPATH,strlen(FULLPATH));
                        strcat(FULLPATH,"/");
                        write(arhiv,"\n",sizeof(char));
                        /* Рекурсивный вызов */
                        input_zip(entry->d_name, zip,arhiv,1) ;
                    }
            }
            else
            {
                if(strcmp(entry->d_name,NAME)==0)
                    {
                        write(arhiv,  "f", sizeof(char));

                        char*Name1 = entry->d_name;
                        strcat(FULLPATH,Name1);
                        int len = strlen(FULLPATH);
                        write(arhiv,&len,sizeof(len));
                        write(arhiv,FULLPATH,strlen(FULLPATH));
                        write(arhiv,"\n",sizeof(char));
                        int aboba=statbuf.st_size;
                        write(arhiv, &aboba, sizeof(aboba));
                        int i = strlen(FULLPATH)-1;
                        read_data(entry->d_name, arhiv);
                        while(FULLPATH[i]!='/')
                        {
                           FULLPATH[i]='\0';
                            i--;
                        }
                    }
            }
        }
    }
    else
        while((entry = readdir(dp)) != NULL) 
        {
            lstat(entry->d_name, &statbuf);
            ptr=&statbuf;
            if (S_ISDIR(statbuf.st_mode)) 
            { 
             /* Находит каталог, но игнорирует . и .. */
                if (strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0)
                    continue;
                
             /* Рекурсивный вызов */
                write(arhiv,  "d", sizeof(char));
                char*Name1 = entry->d_name;
                strcat(FULLPATH,Name1);
                int len = strlen(FULLPATH);
                // char s[256]={0};
                // sprintf(s,"%d",len);
                write(arhiv,&len,sizeof(len));
                write(arhiv,FULLPATH,strlen(FULLPATH));
                strcat(FULLPATH,"/");
                write(arhiv,"\n",sizeof(char));
                input_zip(entry->d_name, zip,arhiv,1);
                FULLPATH[strlen(FULLPATH)-1]='\0';
                int i = strlen(FULLPATH)-1;
                while(FULLPATH[i]!='/')
                {
                    FULLPATH[i]='\0';
                    i--;
                }

            } 
    /* Начинаем архивировать встреченный "не каталог".*/
            else
            {
                write(arhiv,  "f", sizeof(char));
                char*Name1 = entry->d_name;
                strcat(FULLPATH,Name1);
                int len = strlen(FULLPATH);
                // char s[256]={0};
                // sprintf(s,"%d",len);
                write(arhiv,&len,sizeof(len));
                write(arhiv,FULLPATH,strlen(FULLPATH));
                write(arhiv,"\n",sizeof(char));
                int i = strlen(FULLPATH)-1;
                int aboba=statbuf.st_size;
                write(arhiv, &aboba, sizeof(aboba));
                read_data(entry->d_name, arhiv);
                while(FULLPATH[i]!='/')
                {
                    FULLPATH[i]='\0';
                    i--;
                }
            }
    }
    chdir("..");
    closedir(dp);
}


void Rahim(char *PATH)
{
    int arhiv=open(PATH,O_RDONLY);
    char buff;
    char*buff1;
    int flag = 0,namelength=0;                             // 0: считываем stat; 1: считываем имя; 2: считываем содержимое
    char* Name;
    printf("\nin arhv\n");
    while(read(arhiv,&buff,1) == 1)
    {
       switch (buff)
       {
       case 'd':
            read(arhiv,&namelength,sizeof(int));
            Name=(char*)malloc(namelength+1);
            read(arhiv,Name,namelength);
            //strcat(Name,"1");
            printf("%d",mkdir(Name,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH));
           break;
        case 'f':
            read(arhiv,&namelength,sizeof(int));
            Name=(char*)malloc(namelength);
            read(arhiv,Name,namelength);
            int file=open(Name,O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IXOTH|S_IWUSR|S_IWOTH);
            read(arhiv,&buff,1);
            int size;
            char*str=(char*)malloc(256);
            if(buff=='\n')
            {
                read(arhiv,&size,sizeof(int));
            }
            read(arhiv,&buff,1);
            char*buffer = (char*)malloc(size);
            read(arhiv,buffer,size);
            write(file,buffer,size);
           break;
       default:
           break;
       }
       //break;
    }
    // Name="/home/nikitoser/test1";
    // printf("%d",mkdir(Name,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ));
}